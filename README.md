Run command: docker run --name="jenkins-chrome" <name>/jenkins-chrome:latest &

Note ampersand at end of command to run in background as entrypoint is: ENTRYPOINT ["tail", "-f", "/dev/null"]

This allows the container to continue running.

# Docker File
FROM node:10

RUN apt-get update; apt-get clean

RUN apt-get install -y x11vnc

RUN apt-get install -y xvfb

RUN apt-get install -y fluxbox

RUN apt-get install -y wget

RUN apt-get install -y wmctrl

RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list

RUN apt-get update && apt-get -y install google-chrome-stable

RUN npm install --global bower

ENTRYPOINT ["tail", "-f", "/dev/null"]