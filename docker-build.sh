#!/bin/bash

set -e

# The file to store the version information in
VERSION_FILE=~/jobs/dockerVersions.properties

# The version variables used by this build
VERSIONS=(TRANSIT_JENKINS_CHROME)

# Get the specific build tag (if any) from the command line
BUILD_TAG=$1

# If the build tag has been provided, prepend a dash to it
if [ -n "$BUILD_TAG" ]
then
    BUILD_TAG=-$BUILD_TAG
fi

# helper function
function writeVersions {

    # If the file does not exist, create it
    if [ ! -f VERSION_FILE ]
    then
        touch $VERSION_FILE
    fi

    # Write out each version to the versions file
    for VERSION in "${VERSIONS[@]}"
    do
        # Check to see if the version is already in the file
        if [ `cat $VERSION_FILE | grep "^$VERSION" -c` -eq 0 ]
        then
            # If it is not, append it to the file
            echo "$VERSION=${!VERSION}" >> $VERSION_FILE
        else
            # If it is, replace it with the new version
            sed -i "s/^$VERSION=.*$/$VERSION=${!VERSION}/" $VERSION_FILE
        fi

    done
}

function readVersions {
        . ~/jobs/dockerVersions.properties
        echo "Read in values from dockerVersions.properties"
}

# remove the local images
echo "Docker Cleanup"
(docker ps -a | grep "jenkins-chrome" | awk '{print $1}' | xargs --no-run-if-empty docker rm -f) || true
(docker images | grep "jenkins-chrome" | awk '{print $3}' | xargs --no-run-if-empty docker rmi -f) || true

# take version from package.json
VERSION=`cat package.json | python -c "import json,sys;obj=json.load(sys.stdin);print obj['version'];"`
echo "Version $BUILD_TAG$VERSION"
shopt -s nullglob

# currently not using build tag or version, but it can be used if we need it
echo "docker build --file ./docker/Dockerfile --tag=digitaldispatch/jenkins-chrome --tag=digitaldispatch/jenkins-chrome:latest"
docker build --no-cache --file ./docker/Dockerfile --tag="digitaldispatch/jenkins-chrome" --tag="digitaldispatch/jenkins-chrome:latest" .

echo "push digitaldispatch/jenkins-chrome:latest"
docker push digitaldispatch/jenkins-chrome:latest

TRANSIT_JENKINS_CHROME=$VERSION
writeVersions
echo "done docker-build.sh"
